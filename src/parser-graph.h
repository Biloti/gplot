#ifndef _PARSER_H
#define _PARSER_H

/* Estrutura para armazenar parâmetros e configuração. */
typedef struct {

    char *data;
    int text;
    char *output;
    char *title;

    /* First dimension */
    double o1;
    double d1;
    double x1min;
    double x1max;
    char *label1;
    char *unit1;
    int grid1;
    double tic1;

    /* Second dimension */
    double x2min;
    double x2max;
    char *label2;
    char *unit2;
    int grid2;
    double tic2;

    /* Futher options */
    double lw;
    int dotted;
    int style;
    int marker;
    char *color;
    int gui;
    int pdf;
    double width;
    double height;
    int debug;

} p_t;

p_t *parse_command_line (int argc, char **argv);

#endif
