/*  gimage - Plot a seimic section through gnuplot
 *  Copyright (c) 2022 Ricardo Biloti <biloti@unicamp.br>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "parser-image.h"

extern void set_palette (FILE * fp, int index);

int main (int argc, char **argv)
{
    p_t *p;
    FILE *gpltfp;

    if ((p = parse_command_line (argc, argv)) == NULL) {
        return EXIT_FAILURE;
    }

    if (p->debug) {
        gpltfp = stdout;
    } else {
        gpltfp = popen ("gnuplot", "w");
    }

    if (!p->gui) {
        if (p->pdf) {
            fprintf (gpltfp, "set terminal pdfcairo size %.2fcm,%.2fcm\n", p->width, p->height);
            fprintf (gpltfp, "set output \"%s.pdf\"\n", p->output);
        } else {
            fprintf (gpltfp, "set terminal svg enhanced fontscale 1.3 size %i,%i\n", (int) p->width, (int) p->height);
            fprintf (gpltfp, "set output \"%s.svg\"\n", p->output);
        }
    }

    fprintf (gpltfp, "unset key\n");
    fprintf (gpltfp, "set xtics out nomirror\n");
    fprintf (gpltfp, "set ytics out nomirror\n");

    if (p->nocolorbar)
        fprintf (gpltfp, "unset colorbox\n");

    if (p->title != NULL) {
        fprintf (gpltfp, "set title \"%s\"\n", p->title);
    }

    {
        char text[251];
        strcpy (text, "\0");

        if (p->label1) {
            if (p->unit1) {
                snprintf (text, 250, "%s (%s)", p->label1, p->unit1);
            } else {
                snprintf (text, 250, "%s", p->label1);
            }
        } else {
            if (p->unit1) {
                snprintf (text, 250, "(%s)", p->unit1);
            }
        }
        if (strlen (text) > 0) {
            fprintf (gpltfp, "set ylabel \"%s\"\n", text);
        }

        strcpy (text, "\0");
        if (p->label2) {
            if (p->unit2) {
                snprintf (text, 250, "%s (%s)", p->label2, p->unit2);
            } else {
                snprintf (text, 250, "%s", p->label2);
            }
        } else {
            if (p->unit2) {
                snprintf (text, 250, "(%s)", p->unit2);
            }
        }
        if (strlen (text) > 0) {
            fprintf (gpltfp, "set xlabel \"%s\"\n", text);
        }

        if (p->clabel != NULL) {
            fprintf (gpltfp, "set cblabel \"%s\"\n", p->clabel);
        }
    }

    {
        double xmin, xmax;
        xmin = isnan (p->x2min) ? p->o2 : p->x2min;
        xmax = isnan (p->x2max) ? p->o2 + (p->n2 - 1) * p->d2 : p->x2max;
        fprintf (gpltfp, "set xrange [%f:%f]\n", xmin, xmax);

        if (!isnan (p->d2tic)) {
            if (!isnan (p->o2tic)) {
                fprintf (gpltfp, "set xtics %e,%e\n", p->o2tic, p->d2tic);
            } else {
                fprintf (gpltfp, "set xtics %e\n", p->d2tic);
            }
        }

        xmin = isnan (p->x1min) ? p->o1 : p->x1min;
        xmax = isnan (p->x1max) ? p->o1 + (p->n1 - 1) * p->d1 : p->x1max;
        fprintf (gpltfp, "set yrange [%f:%f] \n", xmax, xmin);

        if (!isnan (p->d1tic)) {
            if (!isnan (p->o1tic)) {
                fprintf (gpltfp, "set ytics %e,%e\n", p->o1tic, p->d1tic);
            } else {
                fprintf (gpltfp, "set ytics %e\n", p->d1tic);
            }
        }

        char cbmin[20];
        char cbmax[20];

        if (isnan (p->cbmin))
            strcpy (cbmin, "*");
        else
            sprintf (cbmin, "%e", p->cbmin);

        if (isnan (p->cbmax))
            strcpy (cbmax, "*");
        else
            sprintf (cbmax, "%e", p->cbmax);

        fprintf (gpltfp, "set cbrange [%s:%s] \n", cbmin, cbmax);
    }

    set_palette (gpltfp, p->palette);

    if (p->su) {
        fprintf (gpltfp, "plot \"< sustrip <%s outpar=/dev/null\" binary format=\"%%float\" "
                 "array=%ix%i scan=yx " "origin=(%f,%f) dx=%f dy=%f " "w image\n", p->data, p->n2, p->n1, p->o2, p->o1, p->d2, p->d1);
    } else {
        fprintf (gpltfp, "plot \"%s\" binary format=\"%%float\" "
                 "array=%ix%i scan=yx " "origin=(%f,%f) dx=%f dy=%f " "w image\n", p->data, p->n2, p->n1, p->o2, p->o1, p->d2, p->d1);
    }

    if (p->gui) {
        fprintf (gpltfp, "pause mouse close\n");
    } else {
        if (!p->pdf) {
            fprintf (gpltfp, "set terminal png enhanced size %i,%i\n", (int) p->width, (int) p->height);
            fprintf (gpltfp, "set output \"%s.png\"\n", p->output);
            fprintf (gpltfp, "replot\n");
        }
    }

    pclose (gpltfp);

    return EXIT_SUCCESS;
}
