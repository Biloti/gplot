#include <stdio.h>
#include <stdlib.h>

void cat (FILE * out, char *fname)
{
    FILE *in;
    char buff[200];
    char palfn[1000];

    sprintf (palfn, "%s/%s/%s",
             getenv("HOME"),
             ".config/gplot/palettes",
             fname);

    in = fopen (palfn, "r");
    if (in == NULL){
        fprintf(stderr, "Unable to open palette in %s\n", palfn);
        exit(-1);
    }
    while (fgets (buff, 200, in) != NULL) {
        fputs (buff, out);
    }
}

void set_palette (FILE * fp, int index)
{

    // From: https://github.com/Gnuplotting/gnuplot-palettes
    switch (index) {
    case 1:                    // gray palette
        fprintf (fp, "set palette gray\n");
        break;
    case 2:
        cat (fp, "greys.pal");
        break;
    case 3:
        cat (fp, "bentcoolwarm.pal");
        break;
    case 4:                    //BWR
        fprintf (fp, "set palette defined (0 '#0000FF', 1 '#FFFFFF', 2 '#FF0000')\n");
        break;
    case 5:                    //RWB
        fprintf (fp, "set palette defined (0 '#FF0000', 1 '#FFFFFF', 2 '#0000FF')\n");
        break;
    case 6:
        cat (fp, "viridis.pal");
        break;
    case 7:
        cat (fp, "parula.pal");
        break;
    case 8:
        cat (fp, "sand.pal");
        break;
    case 9:
        cat (fp, "whylrd.pal");
        break;
    case 10:
        cat (fp, "turbo.pal");
        break;
    case 11:
        cat (fp, "jet.pal");
        break;
    case 12:
        cat (fp, "inferno.pal");
        break;
    case 13:
        cat (fp, "magma.pal");
        break;
    case 14:
        cat (fp, "rdylbu.pal");
        break;
    case 15:
        cat (fp, "puor.pal");
        break;
    case 16:
        cat (fp, "spectral.pal");
        break;
    case 17:
        cat (fp, "purples.pal");
        break;
    case 18:
        cat (fp, "plasma.pal");
        break;
    case 19:
        cat (fp, "bones.pal");
        break;
    case 20:
        fprintf (fp, "set palette defined (0 '#FFFFFF', 1 '#0099CC', 2 '#00194D')\n");
        break;
    default:
        fprintf (fp, "set palette gray\n");
    }
}
