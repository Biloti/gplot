/*  ggraph - Plot a grapg through gnuplot
 *  Copyright (c) 2022 Ricardo Biloti <biloti@unicamp.br>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "parser-graph.h"

int main (int argc, char **argv)
{
    p_t *p;
    FILE *gpltfp;
    char style[16];

    if ((p = parse_command_line (argc, argv)) == NULL) {
        return EXIT_FAILURE;
    }

    if (p->debug) {
        gpltfp = stdout;
    } else {
        gpltfp = popen ("gnuplot", "w");
    }

    if (!p->gui) {
        if (p->pdf) {
            fprintf (gpltfp, "set terminal pdfcairo size %.2fcm,%.2fcm\n", p->width, p->height);
            fprintf (gpltfp, "set output \"%s.pdf\"\n", p->output);
        } else {
            fprintf (gpltfp, "set terminal svg enhanced fontscale 1.3 size %i,%i\n", (int) p->width, (int) p->height);
            fprintf (gpltfp, "set output \"%s.svg\"\n", p->output);
        }
    }

    if (p->title != NULL) {
        fprintf (gpltfp, "\nset title \"%s\"\n", p->title);
    }

    {
        char text[1001];
        strcpy (text, "\0");

        if (p->label1) {
            if (p->unit1) {
                snprintf (text, 1000, "%s (%s)", p->label1, p->unit1);
            } else {
                snprintf (text, 1000, "%s", p->label1);
            }
        } else {
            if (p->unit1) {
                snprintf (text, 1000, "(%s)", p->unit1);
            }
        }
        if (strlen (text) > 0) {
            fprintf (gpltfp, "set xlabel \"%s\"\n", text);
        }

        strcpy (text, "\0");
        if (p->label2) {
            if (p->unit2) {
                snprintf (text, 1000, "%s (%s)", p->label2, p->unit2);
            } else {
                snprintf (text, 1000, "%s", p->label2);
            }
        } else {
            if (p->unit2) {
                snprintf (text, 1000, "(%s)", p->unit2);
            }
        }
        if (strlen (text) > 0) {
            fprintf (gpltfp, "set ylabel \"%s\"\n", text);
        }

    }

    fprintf (gpltfp, "unset key\n\n");

    {
        char lim1[20], lim2[20];

        sprintf (lim1, "*");
        sprintf (lim2, "*");
        if (!isnan (p->x1min)) {
            sprintf (lim1, "%e", p->x1min);
        }
        if (!isnan (p->x1max)) {
            sprintf (lim2, "%e", p->x1max);
        }
        fprintf (gpltfp, "set xrange [%s:%s] \n", lim1, lim2);

        sprintf (lim1, "*");
        sprintf (lim2, "*");
        if (!isnan (p->x2min)) {
            sprintf (lim1, "%e", p->x2min);
        }
        if (!isnan (p->x2max)) {
            sprintf (lim2, "%e", p->x2max);
        }
        fprintf (gpltfp, "set yrange [%s:%s] \n", lim1, lim2);
    }

    if (!isnan (p->tic1)) {
        fprintf (gpltfp, "set xtics %e\n", p->tic1);
    }
    if (!isnan (p->tic2)) {
        fprintf (gpltfp, "set ytics %e\n", p->tic2);
    }

    fprintf (gpltfp, "set mxtics 2\n");
    fprintf (gpltfp, "set mytics 2\n");


    if (p->grid1) {
        fprintf (gpltfp, "set grid x\n");
    }
    if (p->grid2) {
        fprintf (gpltfp, "set grid y\n");
    }

    fprintf (gpltfp, "\nset style line 1 lw %f lt %i pt %i lc rgb \"%s\"\n\n", p->lw, (p->dotted ? 0 : 1), p->marker, p->color);

    fprintf (gpltfp, "ox=%e\n", p->o1);
    fprintf (gpltfp, "dx=%e\n", p->d1);


    switch (p->style) {
    case 0:
        strcpy (style, "w l");
        break;
    case 1:
        strcpy (style, "w p");
        break;
    case 2:
        strcpy (style, "w lp");
        break;
    case 3:
        strcpy (style, "smooth csplines");
        break;
    default:
        strcpy (style, "l");
    }

    if (p->text) {
        fprintf (gpltfp, "plot \"%s\" u 1:2 %s ls 1\n", p->data, style);
    } else {
        fprintf (gpltfp, "plot \"%s\" binary format=\"%%float\" " "u ($0*dx + ox):1 %s ls 1\n", p->data, style);
    }


    if (p->gui) {
        fprintf (gpltfp, "pause mouse close\n");
    } else {
        if (!p->pdf) {
            fprintf (gpltfp, "\nset terminal png enhanced size %i,%i\n", (int) p->width, (int) p->height);
            fprintf (gpltfp, "set output \"%s.png\"\n", p->output);
            fprintf (gpltfp, "replot\n");
        }
    }

    pclose (gpltfp);

    return EXIT_SUCCESS;
}
