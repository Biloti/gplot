#ifndef _PARSER_H
#define _PARSER_H

/* Estrutura para armazenar parâmetros e configuração. */
typedef struct {

    char *data;
    char *output;
    char *title;
    int su;

    /* First dimension */
    int n1;
    double o1;
    double d1;
    double o1tic;
    double d1tic;
    double x1min;
    double x1max;
    char *label1;
    char *unit1;

    /* Second dimension */
    int n2;
    double o2;
    double d2;
    double o2tic;
    double d2tic;
    double x2min;
    double x2max;
    char *label2;
    char *unit2;

    /* Futher options */
    int nocolorbar;
    char *clabel;
    double cbmin;
    double cbmax;
    int gui;
    int pdf;
    double width;
    double height;
    int palette;

    int debug;


} p_t;

p_t *parse_command_line (int argc, char **argv);

#endif
