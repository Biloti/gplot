/* Commmand-line options parser */

#include <stdio.h>
#include <glib.h>
#include <math.h>
#include <locale.h>
#include <sys/stat.h>
#include "parser-image.h"
#include "su.h"

p_t *parse_command_line (int argc, char **argv)
{
    static p_t pp;
    p_t *p = NULL;

    static GOptionEntry entries[] = {
        {"data", 'd', 0, G_OPTION_ARG_FILENAME, &pp.data, "data file", NULL},
        {"output", 'o', 0, G_OPTION_ARG_FILENAME, &pp.output, "output prefix (extension will be added automatically)", NULL},
        {"interactive", 'i', 0, G_OPTION_ARG_NONE, &pp.gui, "open iteractive gui", NULL},
        {"pdf", 0, 0, G_OPTION_ARG_NONE, &pp.pdf, "export image as PDF", NULL},
        {"title", 't', 0, G_OPTION_ARG_STRING, &pp.title, "plot title", NULL},
        {"n1", 0, 0, G_OPTION_ARG_INT, &pp.n1, "number of samples in first dimension", NULL},
        {"d1", 0, 0, G_OPTION_ARG_DOUBLE, &pp.d1, "sampling rate in first dimension", "1"},
        {"o1", 0, 0, G_OPTION_ARG_DOUBLE, &pp.o1, "first sample in first dimension", "0"},
        {"o1tic", 0, 0, G_OPTION_ARG_DOUBLE, &pp.o1tic, "first tic mark in first dimension", NULL},
        {"d1tic", 0, 0, G_OPTION_ARG_DOUBLE, &pp.d1tic, "tic marks' sampling rate in first dimension", NULL},
        {"x1min", 0, 0, G_OPTION_ARG_DOUBLE, &pp.x1min, "window begin in first dimension", NULL},
        {"x1max", 0, 0, G_OPTION_ARG_DOUBLE, &pp.x1max, "window end in first dimension", NULL},
        {"label1", 0, 0, G_OPTION_ARG_STRING, &pp.label1, "label for first dimension", NULL},
        {"unit1", 0, 0, G_OPTION_ARG_STRING, &pp.unit1, "unit for first dimension", NULL},
        {"n2", 0, 0, G_OPTION_ARG_INT, &pp.n2, "number of samples in second dimension", NULL},
        {"d2", 0, 0, G_OPTION_ARG_DOUBLE, &pp.d2, "sampling rate in second dimension", "1"},
        {"o2", 0, 0, G_OPTION_ARG_DOUBLE, &pp.o2, "first sample in second dimension", "0"},
        {"o2tic", 0, 0, G_OPTION_ARG_DOUBLE, &pp.o2tic, "first tic mark in second dimension", NULL},
        {"d2tic", 0, 0, G_OPTION_ARG_DOUBLE, &pp.d2tic, "tic marks' sampling rate in second dimension", NULL},
        {"x2min", 0, 0, G_OPTION_ARG_DOUBLE, &pp.x2min, "window begin in second dimension", NULL},
        {"x2max", 0, 0, G_OPTION_ARG_DOUBLE, &pp.x2max, "window end in second dimension", NULL},
        {"label2", 0, 0, G_OPTION_ARG_STRING, &pp.label2, "label for second dimension", NULL},
        {"unit2", 0, 0, G_OPTION_ARG_STRING, &pp.unit2, "unit for second dimension", NULL},
        {"nocolorbar", 0, 0, G_OPTION_ARG_NONE, &pp.nocolorbar, "hide colorbar", NULL},
        {"colorlabel", 0, 0, G_OPTION_ARG_STRING, &pp.clabel, "colorbar label", NULL},
        {"cbmin", 0, 0, G_OPTION_ARG_DOUBLE, &pp.cbmin, "begin value for color scale", NULL},
        {"cbmax", 0, 0, G_OPTION_ARG_DOUBLE, &pp.cbmax, "end value for color scale", NULL},
        {"palette", 'p', 0, G_OPTION_ARG_INT, &pp.palette, "palette", "1"},
        {"width", 'w', 0, G_OPTION_ARG_DOUBLE, &pp.width, "image width (pts for PNG or cm for PDF)", NULL},
        {"height", 'h', 0, G_OPTION_ARG_DOUBLE, &pp.height, "image height (pts for PNG or cm for PDF)", NULL},
        {"debug", 0, 0, G_OPTION_ARG_NONE, &pp.debug, "write gnuplot script to stdout", NULL},
        {NULL}
    };

    GError *error = NULL;
    GOptionContext *parser;

    /* Set a short description for the program */
    parser = g_option_context_new ("- plot a seismic section through gnuplot");

    /* Summary */
    g_option_context_set_summary (parser, "Command-line tool to plot a seismic section");

    /* Description */
    g_option_context_set_description (parser, "Copyright (c) 2022 Ricardo Biloti\n");

    g_option_context_add_main_entries (parser, entries, NULL);

    /* Complain about unknown options */
    g_option_context_set_ignore_unknown_options (parser, FALSE);

    /* Default values */
    pp.data = NULL;
    pp.output = NULL;
    pp.title = NULL;
    pp.su = 0;
    pp.n1 = 0;
    pp.n2 = 0;
    pp.d1 = 1;
    pp.o1 = 0;
    pp.o1tic = NAN;
    pp.d1tic = NAN;
    pp.d2 = 1;
    pp.o2 = 0;
    pp.o2tic = NAN;
    pp.d2tic = NAN;
    pp.x1min = NAN;
    pp.x1max = NAN;
    pp.x2min = NAN;
    pp.x2max = NAN;
    pp.cbmin = NAN;
    pp.cbmax = NAN;
    pp.label1 = NULL;
    pp.unit1 = NULL;
    pp.label2 = NULL;
    pp.unit2 = NULL;
    pp.nocolorbar = 0;
    pp.clabel = NULL;
    pp.gui = 0;
    pp.pdf = 0;
    pp.palette = 1;
    pp.width = 800;
    pp.height = 540;
    pp.debug = 0;

    /* Parse command line */
    setlocale (LC_CTYPE, "pt_BR.utf8");
    if (g_option_context_parse (parser, &argc, &argv, &error) == FALSE) {
        fprintf (stderr, "%s: syntax error\n", argv[0]);
        fprintf (stderr, "Try %s --help\n", argv[0]);
        return NULL;
    }

    if (pp.pdf) {
        if (pp.width > 20) {
            pp.width = 12;
        }
        if (pp.height > 30) {
            pp.height = 8;
        }
    }

    if (pp.data == NULL) {
        fprintf (stderr, "%s: data missing\n", argv[0]);
        fprintf (stderr, "Try %s --help\n", argv[0]);
        return NULL;
    }
    // Check if data is SU
    if (strcmp (pp.data + strlen (pp.data) - 3, ".su") == 0) {
        FILE *fp;
        suheader_t hdr;

        pp.su = 1;
        fp = fopen (pp.data, "r");
        fread (&hdr, sizeof (suheader_t), 1, fp);
        fclose (fp);

        pp.n1 = hdr.ns;
        pp.o1 = hdr.delrt * 1.0e-3;
        if (hdr.dt != 0) {
            pp.d1 = hdr.dt * 1.0e-6;
        } else if (hdr.d1 != 0)
            pp.d1 = hdr.d1;
    }

    if (pp.output == NULL) {
        pp.output = pp.data;
    }

    if (pp.n1 == 0) {
        fprintf (stderr, "%s: n1 missing\n", argv[0]);
        fprintf (stderr, "Try %s --help\n", argv[0]);
        return NULL;
    }

    if (pp.n2 == 0) {
        struct stat sb;

        stat (pp.data, &sb);

        if (pp.su) {
            pp.n2 = sb.st_size / (sizeof (suheader_t) + sizeof (float) * pp.n1);
            if ((pp.n2 * (sizeof (suheader_t) + sizeof (float) * pp.n1) - sb.st_size) != 0) {
                fprintf (stderr, "Problably n1 = %i and n2=%i isn't quite right\n", pp.n1, pp.n2);
            }

        } else {
            pp.n2 = sb.st_size / (sizeof (float) * pp.n1);
            if ((pp.n2 * sizeof (float) * pp.n1 - sb.st_size) != 0) {
                fprintf (stderr, "Problably n1 = %i and n2=%i isn't quite right\n", pp.n1, pp.n2);
            }
        }
    }

    p = (p_t *) malloc (sizeof (p_t));
    memcpy (p, &pp, sizeof (p_t));

    g_option_context_free (parser);

    return p;
}
