/* Commmand-line options parser */

#include <stdio.h>
#include <glib.h>
#include <math.h>
#include <locale.h>
#include "parser-graph.h"

p_t *parse_command_line (int argc, char **argv)
{
    static p_t pp;
    p_t *p = NULL;

    static GOptionEntry entries[] = {
        {"data", 'd', 0, G_OPTION_ARG_FILENAME, &pp.data, "data file", NULL},
        {"text", 't', 0, G_OPTION_ARG_NONE, &pp.text, "two-columns text data file (binary is default)", NULL},
        {"output", 'o', 0, G_OPTION_ARG_FILENAME, &pp.output, "output prefix (extension will be added automatically)", NULL},
        {"interactive", 'i', 0, G_OPTION_ARG_NONE, &pp.gui, "open iteractive gui", NULL},
        {"pdf", 0, 0, G_OPTION_ARG_NONE, &pp.pdf, "export image as PDF", NULL},
        {"title", 0, 0, G_OPTION_ARG_STRING, &pp.title, "plot title", NULL},
        {"o1", 0, 0, G_OPTION_ARG_DOUBLE, &pp.o1, "first sample in first dimension", "0"},
        {"d1", 0, 0, G_OPTION_ARG_DOUBLE, &pp.d1, "sampling rate in first dimension", "1"},
        {"x1min", 0, 0, G_OPTION_ARG_DOUBLE, &pp.x1min, "window begin in first dimension", NULL},
        {"x1max", 0, 0, G_OPTION_ARG_DOUBLE, &pp.x1max, "window end in first dimension", NULL},
        {"label1", 0, 0, G_OPTION_ARG_STRING, &pp.label1, "label for first dimension", NULL},
        {"unit1", 0, 0, G_OPTION_ARG_STRING, &pp.unit1, "unit for first dimension", NULL},
        {"grid1", 0, 0, G_OPTION_ARG_NONE, &pp.grid1, "show grid lines for first axis", NULL},
        {"tic1", 0, 0, G_OPTION_ARG_DOUBLE, &pp.tic1, "tic interval for first axis", NULL},
        {"x2min", 0, 0, G_OPTION_ARG_DOUBLE, &pp.x2min, "window begin in second dimension", NULL},
        {"x2max", 0, 0, G_OPTION_ARG_DOUBLE, &pp.x2max, "window end in second dimension", NULL},
        {"label2", 0, 0, G_OPTION_ARG_STRING, &pp.label2, "label for second dimension", NULL},
        {"unit2", 0, 0, G_OPTION_ARG_STRING, &pp.unit2, "unit for second dimension", NULL},
        {"grid2", 0, 0, G_OPTION_ARG_NONE, &pp.grid2, "show grid lines for second axis", NULL},
        {"tic2", 0, 0, G_OPTION_ARG_DOUBLE, &pp.tic2, "tic interval for second axis", NULL},
        {"color", 'c', 0, G_OPTION_ARG_STRING, &pp.color, "RGB color (000000, for black, FF0000 for red, ...)", "#000000"},
        {"style", 's', 0, G_OPTION_ARG_INT, &pp.style, "style (0 - line, 1 - marks, 2 - line and marks, 3 - cspline)", "0"},
        {"linewidth", 'l', 0, G_OPTION_ARG_DOUBLE, &pp.lw, "line width", "1"},
        {"dotted", 0, 0, G_OPTION_ARG_NONE, &pp.dotted, "dotted instead of solid line", NULL},
        {"marker", 'm', 0, G_OPTION_ARG_INT, &pp.marker, "marker index", "2"},
        {"width", 'W', 0, G_OPTION_ARG_DOUBLE, &pp.width, "image width (pts for PNG or cm for PDF)", NULL},
        {"height", 'H', 0, G_OPTION_ARG_DOUBLE, &pp.height, "image height (pts for PNG or cm for PDF)", NULL},
        {"debug", 0, 0, G_OPTION_ARG_NONE, &pp.debug, "write gnuplot script to stdout and exit", NULL},
        {NULL}
    };

    GError *error = NULL;
    GOptionContext *parser;

    /* Set a short description for the program */
    parser = g_option_context_new ("- plot graph through gnuplot");

    /* Summary */
    g_option_context_set_summary (parser, "Command-line tool to plot a graph");

    /* Description */
    g_option_context_set_description (parser, "Copyright (c) 2022 Ricardo Biloti\n");

    g_option_context_add_main_entries (parser, entries, NULL);

    /* Complain about unknown options */
    g_option_context_set_ignore_unknown_options (parser, FALSE);

    /* Default values */
    pp.data = NULL;
    pp.text = 0;
    pp.output = NULL;
    pp.gui = 0;
    pp.pdf = 0;

    pp.title = NULL;
    pp.o1 = 0;
    pp.d1 = 1;
    pp.x1min = NAN;
    pp.x1max = NAN;
    pp.label1 = NULL;
    pp.unit1 = NULL;
    pp.grid1 = 0;
    pp.tic1 = NAN;

    pp.x2min = NAN;
    pp.x2max = NAN;
    pp.label2 = NULL;
    pp.unit2 = NULL;
    pp.grid2 = 0;
    pp.tic2 = NAN;

    pp.color = malloc (8);
    strcpy (pp.color, "#000000");
    pp.style = 0;
    pp.lw = 1;
    pp.dotted = 0;
    pp.marker = 2;

    pp.width = 800;
    pp.height = 450;
    pp.debug = 0;

    /* Parse command line */
    setlocale (LC_CTYPE, "pt_BR.utf8");
    if (g_option_context_parse (parser, &argc, &argv, &error) == FALSE) {
        fprintf (stderr, "%s: syntax error\n", argv[0]);
        fprintf (stderr, "Try %s --help\n", argv[0]);
        return NULL;
    }

    if (pp.pdf) {
        if (pp.width > 20) {
            pp.width = 12;
        }
        if (pp.height > 30) {
            pp.height = 8;
        }
    }

    if (pp.data == NULL) {
        fprintf (stderr, "%s: data missing\n", argv[0]);
        fprintf (stderr, "Try %s --help\n", argv[0]);
        return NULL;
    }

    if (pp.output == NULL) {
        pp.output = pp.data;
    }

    p = (p_t *) malloc (sizeof (p_t));
    memcpy (p, &pp, sizeof (p_t));

    g_option_context_free (parser);

    return p;
}
