#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main (int argc, char **argv)
{
    int n1;
    float *buff;

    if (argc < 2) {
        fprintf (stderr, "Usage: %s n1 <data.bin >data.dat\n", argv[0]);
        return EXIT_FAILURE;
    }

    n1 = atoi (argv[1]);
    buff = (float *) malloc (sizeof (float) * n1);

    while (n1 == (int) fread (buff, sizeof (float), n1, stdin)) {
        for (int j = 0; j < n1; j++) {
            fprintf (stdout, "%e ", buff[j]);
        }
        fprintf (stdout, "\n");
    }

    free (buff);
    return EXIT_SUCCESS;
}
