from setuptools import setup

setup(
    name='gplot',
    version='0.1.0',
    install_requires=[
	    'requests',
	    'importlib-metadata; python_version > "3.0"',
        ],
    )
