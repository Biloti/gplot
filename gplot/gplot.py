"""
Gplot is a set of tools to display data as image or graph,
based on Gnuplot.

Copyright 2022 (c) Ricardo Biloti <biloti@unicamp.br>
"""

import functools
import subprocess
import math
import re
from os import getcwd, path
import ipywidgets as W
from IPython.core.display import display, HTML


#--------------------------------------------------------------------------------
def gwiggle(data, n1=None, n2=0,
          title=None,
          o1=0, d1=1, o2=0, d2=1,
          x1min=math.nan, x1max=math.nan,
          x2min=math.nan, x2max=math.nan,
          label1=None, unit1=None,
          label2=None, unit2=None,
          o1tic=None, d1tic=None,
          o2tic=None, d2tic=None,
          scale=1,
          width=1024, height=600, pdfwidth=12, pdfheight=10,
          dialog=False, gui=True):
    """gwiggle (data, n1=None, n2=None, title=None, o1=0, d1=1, o2=0, d2=1,
        x1min=math.nan, x1max=math.nan, x2min=math.nan, x2max=math.nan,
        label1=None, unit1=None, label2=None, unit2=None,
        scale=1, diaolog=False, gui=True)

Plot a binary data file as wiggles.

data  - a single-precision binary data filenane
n1    - number of samples in first dimension
n2    - number of samples in second dimension (0 for autodetect)

In case data is data is raw binary, n1 is required. In the
special case of a RSF data file, n1 and n2 are read from header.

o1    - origin for first axis
d1    - sampling interval in the first axis
o2    - origin for second axis
d2    - sampling interval in the second axis
x1min and x1max  - for windowing in the first axis
x2min and x2max  - for windowing in the second axis
label1 and unit1 - for composeing label for first axis
label2 and unit2 - for composeing label for second axis
o1tic and d1tic  - first tic mark and space between tics for first axis
o2tic and d2tic  - first tic mark and space between tics for second axis
scale            - scale factor for trace amplitudes
dialog           - for interactive external dialog
gui              - for display UI
    """

    def parse_rsf(rsffn):

        pattern = re.compile('^[ 	]*([a-z].*)=(.*)$')
        lines = open(rsffn).readlines()
        header = {}
        for line in lines:
            match = pattern.match(line)
            if match:
                header[match.group(1)] = match.group(2)

        return header

    def on_config_clicked(button, widget):

        if widget['configp'].layout.display == 'none':
            widget['configp'].layout.display = 'flex'
            button.icon = 'caret-up'
        else:
            widget['configp'].layout.display = 'none'
            button.icon = 'caret-down'

    def on_reload_clicked(button, widget):

        fname = widget['input'].value
        if (not fname) or fname[-4:] != '.rsf':
            return

        if not path.exists(fname):
            widget['statusbar'].value = f"<b>Last error message:</b> {fname} not found"
            widget['statusbar'].layout.display = 'flex'
            return

        widget['header'] = parse_rsf(fname)
        for intkey in ['n1','n2']:
            if intkey in widget['header']:
                widget[intkey].value = int(widget['header'][intkey])

        for floatkey in ['d1','d2','o1','o2']:
            if floatkey in widget['header']:
                widget[floatkey].value = float(widget['header'][floatkey])

        for strkey in ['label1','label2','unit1','unit2']:
            if strkey in widget['header']:
                widget[strkey].value = widget['header'][strkey].replace('"', '')


    def on_clearw_clicked(button, widget):

        widget['x1min'].value = math.nan
        widget['x1max'].value = math.nan
        widget['x2min'].value = math.nan
        widget['x2max'].value = math.nan

    def on_plot_clicked(button, widget, pdf, gui):
        if not widget['input'].value:
            return

        if widget['input'].value[-3:] == '.su':
            SU=True
        else:
            SU=False

        if not path.exists(widget['input'].value):
            widget['statusbar'].value = f"<b>Last error message:</b> {widget['input'].value} not found"
            widget['statusbar'].layout.display = 'flex'
            return

        if (pdf and widget['dialog'].value):
            return

        if widget['input'].value[-4:] == '.rsf':
            fname = widget['header']['in'].replace('"', '')
        else:
            fname = widget['input'].value

        if not path.exists(fname):
            widget['statusbar'].value = f"<b>Last error message:</b> {fname} not found"
            widget['statusbar'].layout.display = 'flex'
            return

        if widget['input'].value != '/':
            cwd = getcwd()
        else:
            cwd = ''

        if pdf:
            fn = widget['input'].value + '.pdf'
            output = path.join(cwd,fn)
            width = widget['pdfwidth'].value
            height = widget['pdfheight'].value
            widget['statusbar'].value = 'Export to <a href="file://' + \
                                        output + '">' + fn + '</a>'
            widget['statusbar'].layout.display = 'flex'
        else:
            fnsvg = widget['input'].value + '.svg'
            fnpng = widget['input'].value + '.png'
            outputsvg = path.join(cwd,fnsvg)
            outputpng = path.join(cwd,fnpng)
            width = widget['width'].value
            height = widget['height'].value
            if not widget['dialog'].value:
                widget['statusbar'].value = 'Saved as <a href="file://' + \
                                            outputsvg + '">' + fnsvg + '</a> ' + \
                                            'and exported to <a href="file://' + \
                                            outputpng + '">' + fnpng + '</a>'
                widget['statusbar'].layout.display = 'flex'
            else:
                widget['statusbar'].value = ''
                widget['statusbar'].layout.display = 'none'

        cmdline = f"gwiggle --data={fname} " + \
            f"--output={widget['input'].value} " +\
            f"--o1={widget['o1'].value} --o2={widget['o2'].value} " + \
            f"--d1={widget['d1'].value} --d2={widget['d2'].value} " + \
            f"--label1=\"{widget['label1'].value}\" --label2=\"{widget['label2'].value}\" " + \
            f"--title=\"{widget['title'].value}\" " + \
            f"--width={width} --height={height} --scale={widget['scale'].value} "

        if pdf:
            cmdline = cmdline + '--pdf '

        if widget['unit1'].value:
            cmdline = cmdline + f"--unit1={widget['unit1'].value} "

        if widget['unit2'].value:
            cmdline = cmdline + f"--unit2={widget['unit2'].value} "

        if widget['n1'].value != 0:
            cmdline = cmdline + f"--n1={widget['n1'].value} "
        elif not SU:
            widget['statusbar'].value = "<b>Last error message:</b> n1 missing"
            widget['statusbar'].layout.display = 'flex'
            return

        if widget['n2'].value != 0:
            cmdline = cmdline + f"--n2={widget['n2'].value} "

        if not math.isnan(widget['x1min'].value):
            cmdline = cmdline + f"--x1min={widget['x1min'].value} "

        if not math.isnan(widget['x1max'].value):
            cmdline = cmdline + f"--x1max={widget['x1max'].value} "

        if not math.isnan(widget['x2min'].value):
            cmdline = cmdline + f"--x2min={widget['x2min'].value} "

        if not math.isnan(widget['x2max'].value):
            cmdline = cmdline + f"--x2max={widget['x2max'].value} "

        if widget['dialog'].value:
            cmdline = cmdline + "--interactive "

        widget['statusbar'].value = widget['statusbar'].value

        run = subprocess.run([cmdline], shell=True, text=False)

        if not pdf and gui and not widget['dialog'].value:
            if len(widget['imgw'].children) == 0:
                widget['image'] = W.Image(layout = W.Layout(width="100%"))
                widget['imgw'].children = (widget['image'],)

#            widget['image'].format = 'svg+xml'
#            widget['image'].value = open(widget['input'].value+".svg","rb").read()

            widget['image'].format = 'png'
            widget['image'].value = open(widget['input'].value+".png","rb").read()


    #-----------------------------------------------------------------------------

    widget = {}
    widget['header'] = {}

    full = W.Layout(width="98%")
    small = W.Layout(width="70px")
    medium = W.Layout(width="180px")
    lo_iconbt = W.Layout(width="30px", indent=False)
    lo_label = W.Layout(width="120px")
    lo_label2 = W.Layout(width="220px", display="flex", justify_content="flex-end")
    lo_label3 = W.Layout(display="flex", justify_content="flex-end")
    lo_label4 = W.Layout(width="120px", display="flex", justify_content="flex-end")
    lo_check = W.Layout(width="120px", display="flex", justify_content="flex-start")

    # data file
    widget['input'] = W.Text(placeholder="RSF or Binary data file",
                             value = data,layout=medium)

    # reload from rsf button
    widget['reload'] = W.Button(tooltip="Reload header info for RSF files",
                                icon="refresh", layout = lo_iconbt)
    widget['reload'].on_click(functools.partial(on_reload_clicked, widget=widget))
    # n1
    widget['n1'] = W.IntText(layout=small)
    if n1 is not None:
        widget['n1'].value = n1

    # n2
    widget['n2'] = W.IntText(layout=small)
    widget['n2'].value = n2

    # config button
    widget['config'] = W.Button(description="Config",
                                button_style="info",
                                icon="caret-down", indent=False)
    widget['config'].on_click(functools.partial(on_config_clicked, widget=widget))

    # plot button
    widget['plot'] = W.Button(description="Plot", button_style="danger",indent=False)
    widget['plot'].on_click(functools.partial(on_plot_clicked, widget=widget, pdf=False, gui=gui))

    # plot button
    widget['pdf'] = W.Button(description="Export to PDF", button_style="danger",indent=False)
    widget['pdf'].on_click(functools.partial(on_plot_clicked, widget=widget, pdf=True, gui=gui))

    #----------------------------------------------------------------------------
    # config panel

    # title
    widget['title'] = W.Text(placeholder="Title", layout=medium)
    if title is not None:
        widget['title'].value = title

    titleline = W.HBox([W.Label(value="Title", layout=lo_label),
                        widget['title']])

    # Axis 1
    n1clone = W.FloatText(layout=small)
    link1 = W.link((widget['n1'],'value'),(n1clone,'value'))

    widget['o1'] = W.FloatText(layout=small, value = o1)
    widget['d1'] = W.FloatText(layout=small, value = d1)
    widget['label1'] = W.Text(placeholder="Label",layout=medium)
    if label1 is not None:
        widget['label1'].value = label1

    widget['unit1'] = W.Text(placeholder="Unit",layout=medium)
    if unit1 is not None:
        widget['unit1'].value = unit1

    axis1 = W.HBox([W.Label(value="Vertical axis", layout=lo_label),
                    widget['label1'],
                    widget['unit1'],
                    W.Label(value="origin / sampling / samples", layout=lo_label2),
                    widget['o1'],
                    widget['d1'],
                    n1clone])

    # Axis 2
    n2clone = W.FloatText(layout=small)
    link2 = W.link((widget['n2'],'value'),(n2clone,'value'))
    widget['o2'] = W.FloatText(layout=small, value = o2)
    widget['d2'] = W.FloatText(layout=small, value = d2)
    widget['label2'] = W.Text(placeholder="Label",layout=medium)
    if label2 is not None:
        widget['label2'].value = label2

    widget['unit2'] = W.Text(placeholder="Unit",layout=medium)
    if unit2 is not None:
        widget['unit2'].value = unit2

    axis2 = W.HBox([W.Label(value="Horizontal axis", layout=lo_label),
                    widget['label2'],
                    widget['unit2'],
                    W.Label(value="origin / sampling / samples", layout=lo_label2),
                    widget['o2'],
                    widget['d2'],
                    n2clone])
    # Window
    lo_wind = small
    widget['x1min'] = W.FloatText(layout=lo_wind, value = x1min)
    widget['x1max'] = W.FloatText(layout=lo_wind, value = x1max)
    widget['x2min'] = W.FloatText(layout=lo_wind, value = x2min)
    widget['x2max'] = W.FloatText(layout=lo_wind, value = x2max)
    widget['clearw'] = W.Button(tooltip="Clear window range",
                                icon="trash", layout = lo_iconbt)

    widget['clearw'].on_click(functools.partial(on_clearw_clicked, widget=widget))

    window = W.HBox([W.Label(value="Window", layout=lo_label),
                     W.Label(value="vertical range:", layout=lo_label3),
                     widget['x1min'],
                     widget['x1max'],
                     W.Label(value="horizontal range:", layout=lo_label4),
                     widget['x2min'],
                     widget['x2max'],
                     widget['clearw']])

    # Further options
    widget['width'] = W.IntText(value=width, layout=small)
    widget['height'] = W.IntText(value=height, layout=small)

    widget['pdfwidth'] = W.FloatText(value=pdfwidth, step=0.5, layout=small)
    widget['pdfheight'] = W.FloatText(value=pdfheight, step=0.5, layout=small)

    widget['dialog'] = W.Checkbox(description="Interactive",
                                  value=dialog,
                                  layout = lo_check, indent=False)

    widget['scale'] = W.FloatText(value=scale, step=0.1, layout=small)

    controls = W.HBox([W.Label(value="Image size (w/h)",layout=lo_label),
                       widget['width'],
                       widget['height'],
                       W.Label(value="Size of pdf (cm)",
                               layout=W.Layout(width="100px",
                                               display="flex",
                                               justify_content="flex-end")),
                       widget['pdfwidth'],
                       widget['pdfheight'],
                       W.Label(value="scale factor:",
                               layout=W.Layout(width="100px",
                                               display="flex",
                                               justify_content="flex-end")),
                       widget['scale'],
                       widget['dialog']])


    widget['configp'] = W.VBox([controls,titleline,axis1,axis2,window])
    widget['configp'].layout.display = 'none'

    # image area
    widget['imgw'] = W.HBox(layout=full)

    # toolbar
    toolbar = W.HBox([widget['input'],
                      widget['reload'],
                      widget['n1'],
                      widget['n2'],
                      widget['config'],
                      widget['plot'],
                      widget['pdf']], layout=full)


    # statusbar
    widget['statusbar'] = W.HTML()
    widget['statusbar'].layout = W.Layout(width="98%",
                                          border = "1px solid",
                                          padding = "2px")
    widget['statusbar'].add_class("statusbar")
    widget['statusbar'].layout.display = 'none'

    widget['main'] = W.VBox([toolbar,
                             widget['configp'],
                             widget['imgw'],
                             widget['statusbar']])

    if path.exists(data):
        on_reload_clicked(widget['reload'], widget)
    else:
        widget['statusbar'].value = f"<b>Last error message:</b> {data} not found"
        widget['statusbar'].layout.display = 'flex'

    display(HTML("""
       <style>
         .statusbar {border-radius: 4px; background: #f0f0f0;}
       </style>"""))

    if gui:
        display(widget['main'])

    on_plot_clicked(widget['plot'], widget, False, gui)

#--------------------------------------------------------------------------------
def gimage(data, n1=None, n2=0,
          title=None,
          o1=0, d1=1, o2=0, d2=1,
          x1min=math.nan, x1max=math.nan,
          x2min=math.nan, x2max=math.nan,
          label1=None, unit1=None,
          label2=None, unit2=None,
          o1tic=None, d1tic=None,
          o2tic=None, d2tic=None,
          scale_bar=True, clabel=None, cbmin=math.nan, cbmax=math.nan,
          width=1024, height=420, pdfwidth=12, pdfheight=7,
          palette=1, dialog=False, gui=True):
    """gimage (data, n1=None, n2=None, title=None, o1=0, d1=1, o2=0, d2=1,
        x1min=math.nan, x1max=math.nan, x2min=math.nan, x2max=math.nan,
        label1=None, unit1=None, label2=None, unit2=None,
        scale_bar=True, clabel=None, cbmin=math.nan, cbmax=math.nan, palette=1,
        gui=False)

Shows a binary data file as an image.

data  - a single-precision binary data filenane
n1    - number of samples in first dimension
n2    - number of samples in second dimension (0 for autodetect)

In case data is data is raw binary, n1 and n2 are required. In the
special case of a RSF data file, n1 and n2 are read from header.

o1    - origin for first axis
d1    - sampling interval in the first axis
o2    - origin for second axis
d2    - sampling interval in the second axis
x1min and x1max  - for windowing in the first axis
x2min and x2max  - for windowing in the second axis
label1 and unit1 - for composing label for first axis
label2 and unit2 - for composing label for second axis
o1tic and d1tic  - first tic mark and space between tics for first axis
o2tic and d2tic  - first tic mark and space between tics for second axis
scale_bar        - show/hide color bar
clabel           - label for color bar
cbmin and cbmax  - for windowing amplitudes in image
palette          - index for color palette (1,..,19)
dialog           - for interactive external dialog
gui              - for display UI
    """

    def parse_rsf(rsffn):

        pattern = re.compile('^[ 	]*([a-z].*)=(.*)$')
        lines = open(rsffn).readlines()
        header = {}
        for line in lines:
            match = pattern.match(line)
            if match:
                header[match.group(1)] = match.group(2)

        return header

    def on_config_clicked(button, widget):

        if widget['configp'].layout.display == 'none':
            widget['configp'].layout.display = 'flex'
            button.icon = 'caret-up'
        else:
            widget['configp'].layout.display = 'none'
            button.icon = 'caret-down'

    def on_reload_clicked(button, widget):

        fname = widget['input'].value
        if (not fname) or fname[-4:] != '.rsf':
            return

        if not path.exists(fname):
            widget['statusbar'].value = f"<b>Last error message:</b> {fname} not found"
            widget['statusbar'].layout.display = 'flex'
            return

        widget['header'] = parse_rsf(fname)
        for intkey in ['n1','n2']:
            if intkey in widget['header']:
                widget[intkey].value = int(widget['header'][intkey])

        for floatkey in ['d1','d2','o1','o2']:
            if floatkey in widget['header']:
                widget[floatkey].value = float(widget['header'][floatkey])

        for strkey in ['label1','label2','unit1','unit2']:
            if strkey in widget['header']:
                widget[strkey].value = widget['header'][strkey].replace('"', '')


    def on_clearw_clicked(button, widget):

        widget['x1min'].value = math.nan
        widget['x1max'].value = math.nan
        widget['x2min'].value = math.nan
        widget['x2max'].value = math.nan
        widget['cbmin'].value = math.nan
        widget['cbmax'].value = math.nan

    def on_plot_clicked(button, widget, pdf, gui):
        if not widget['input'].value:
            return

        if widget['input'].value[-3:] == '.su':
            SU=True
        else:
            SU=False

        if not path.exists(widget['input'].value):
            widget['statusbar'].value = f"<b>Last error message:</b> {widget['input'].value} not found"
            widget['statusbar'].layout.display = 'flex'
            return

        if (pdf and widget['dialog'].value):
            return

        if widget['input'].value[-4:] == '.rsf':
            fname = widget['header']['in'].replace('"', '')
        else:
            fname = widget['input'].value

        if not path.exists(fname):
            widget['statusbar'].value = f"<b>Last error message:</b> {fname} not found"
            widget['statusbar'].layout.display = 'flex'
            return

        if widget['input'].value != '/':
            cwd = getcwd()
        else:
            cwd = ''

        if pdf:
            fn = widget['input'].value + '.pdf'
            output = path.join(cwd,fn)
            width = widget['pdfwidth'].value
            height = widget['pdfheight'].value
            widget['statusbar'].value = 'Export to <a href="file://' + \
                                        output + '">' + fn + '</a>'
            widget['statusbar'].layout.display = 'flex'
        else:
            fnsvg = widget['input'].value + '.svg'
            fnpng = widget['input'].value + '.png'
            outputsvg = path.join(cwd,fnsvg)
            outputpng = path.join(cwd,fnpng)
            width = widget['width'].value
            height = widget['height'].value
            if not widget['dialog'].value:
                widget['statusbar'].value = 'Saved as <a href="file://' + \
                                            outputsvg + '">' + fnsvg + '</a> ' + \
                                            'and exported to <a href="file://' + \
                                            outputpng + '">' + fnpng + '</a>'
                widget['statusbar'].layout.display = 'flex'
            else:
                widget['statusbar'].value = ''
                widget['statusbar'].layout.display = 'none'

        cmdline = f"gimage --data={fname} " + \
            f"--output={widget['input'].value} " + \
            f"--o1={widget['o1'].value} --o2={widget['o2'].value} " + \
            f"--d1={widget['d1'].value} --d2={widget['d2'].value} " + \
            f"--label1=\"{widget['label1'].value}\" --label2=\"{widget['label2'].value}\" " + \
            f"--title=\"{widget['title'].value}\" " + \
            f"--colorlabel=\"{widget['scalelabel'].value}\" " + \
            f"--width={width} --height={height} " + \
            f"--palette={widget['palette'].value} "

        if pdf:
            cmdline = cmdline + '--pdf '

        if widget['unit1'].value:
            cmdline = cmdline + f"--unit1={widget['unit1'].value} "

        if widget['unit2'].value:
            cmdline = cmdline + f"--unit2={widget['unit2'].value} "

        if widget['n1'].value != 0:
            cmdline = cmdline + f"--n1={widget['n1'].value} "
        elif not SU:
            widget['statusbar'].value = "<b>Last error message:</b> n1 missing"
            widget['statusbar'].layout.display = 'flex'
            return

        if widget['n2'].value != 0:
            cmdline = cmdline + f"--n2={widget['n2'].value} "

        if not math.isnan(widget['x1min'].value):
            cmdline = cmdline + f"--x1min={widget['x1min'].value} "

        if not math.isnan(widget['x1max'].value):
            cmdline = cmdline + f"--x1max={widget['x1max'].value} "

        if not math.isnan(widget['x2min'].value):
            cmdline = cmdline + f"--x2min={widget['x2min'].value} "

        if not math.isnan(widget['x2max'].value):
            cmdline = cmdline + f"--x2max={widget['x2max'].value} "

        if not math.isnan(widget['cbmin'].value):
            cmdline = cmdline + f"--cbmin={widget['cbmin'].value} "

        if not math.isnan(widget['cbmax'].value):
            cmdline = cmdline + f"--cbmax={widget['cbmax'].value} "
            
        if widget['o1tic'] is not None:
            cmdline = cmdline + f"--o1tic={widget['o1tic']} "

        if widget['d1tic'] is not None:
            cmdline = cmdline + f"--d1tic={widget['d1tic']} "

        if widget['o2tic'] is not None:
            cmdline = cmdline + f"--o2tic={widget['o2tic']} "

        if widget['d2tic'] is not None:
            cmdline = cmdline + f"--d2tic={widget['d2tic']} "

        if not widget['scalebar'].value:
            cmdline = cmdline + "--nocolorbar "

        if widget['dialog'].value:
            cmdline = cmdline + "--interactive "

        run = subprocess.run([cmdline], shell=True, text=False, check=True)

        if not pdf and gui and not widget['dialog'].value:
            if len(widget['imgw'].children) == 0:
                widget['image'] = W.Image(layout = W.Layout(width="100%"))
                widget['imgw'].children = (widget['image'],)

#            widget['image'].format = 'svg+xml'
#            widget['image'].value = open(widget['input'].value+".svg","rb").read()

            widget['image'].format = 'png'
            widget['image'].value = open(widget['input'].value+".png","rb").read()

    #-----------------------------------------------------------------------------

    widget = {}
    widget['header'] = {}

    full = W.Layout(width="98%")
    small = W.Layout(width="70px")
    medium = W.Layout(width="180px")
    lo_iconbt = W.Layout(width="30px", indent=False)
    lo_label = W.Layout(width="120px")
    lo_label2 = W.Layout(width="220px", display="flex", justify_content="flex-end")
    lo_label3 = W.Layout(display="flex", justify_content="flex-end")
    lo_label4 = W.Layout(width="120px", display="flex", justify_content="flex-end")
    lo_check = W.Layout(width="120px", display="flex", justify_content="flex-start")

    # data file
    widget['input'] = W.Text(placeholder="RSF or Binary data file",
                             value = data,layout=medium)

    # reload from rsf button
    widget['reload'] = W.Button(tooltip="Reload header info for RSF files",
                                icon="refresh", layout = lo_iconbt)
    widget['reload'].on_click(functools.partial(on_reload_clicked, widget=widget))
    # n1
    widget['n1'] = W.IntText(layout=small)
    if n1 is not None:
        widget['n1'].value = n1

    # n2
    widget['n2'] = W.IntText(layout=small)
    widget['n2'].value = n2

    # config button
    widget['config'] = W.Button(description="Config",
                                button_style="info",
                                icon="caret-down", indent=False)
    widget['config'].on_click(functools.partial(on_config_clicked, widget=widget))

    # plot button
    widget['plot'] = W.Button(description="Plot", button_style="danger",indent=False)
    widget['plot'].on_click(functools.partial(on_plot_clicked, widget=widget, pdf=False, gui=gui))

    # plot button
    widget['pdf'] = W.Button(description="Export to PDF", button_style="danger",indent=False)
    widget['pdf'].on_click(functools.partial(on_plot_clicked, widget=widget, pdf=True, gui=gui))

    #----------------------------------------------------------------------------
    # config panel

    # title
    widget['title'] = W.Text(placeholder="Title", layout=medium)
    if title is not None:
        widget['title'].value = title

    titleline = W.HBox([W.Label(value="Title", layout=lo_label),
                        widget['title']])

    # Axis 1
    n1clone = W.FloatText(layout=small)
    link1 = W.link((widget['n1'],'value'),(n1clone,'value'))

    widget['o1'] = W.FloatText(layout=small, value = o1)
    widget['d1'] = W.FloatText(layout=small, value = d1)

    widget['o1tic'] = o1tic
    widget['d1tic'] = d1tic

    widget['label1'] = W.Text(placeholder="Label",layout=medium)
    if label1 is not None:
        widget['label1'].value = label1

    widget['unit1'] = W.Text(placeholder="Unit",layout=medium)
    if unit1 is not None:
        widget['unit1'].value = unit1

    axis1 = W.HBox([W.Label(value="Vertical axis", layout=lo_label),
                    widget['label1'],
                    widget['unit1'],
                    W.Label(value="origin / sampling / samples", layout=lo_label2),
                    widget['o1'],
                    widget['d1'],
                    n1clone])

    # Axis 2
    n2clone = W.FloatText(layout=small)
    link2 = W.link((widget['n2'],'value'),(n2clone,'value'))
    widget['o2'] = W.FloatText(layout=small, value = o2)
    widget['d2'] = W.FloatText(layout=small, value = d2)
    
    widget['o2tic'] = o2tic
    widget['d2tic'] = d2tic
    
    widget['label2'] = W.Text(placeholder="Label",layout=medium)
    if label2 is not None:
        widget['label2'].value = label2

    widget['unit2'] = W.Text(placeholder="Unit",layout=medium)
    if unit2 is not None:
        widget['unit2'].value = unit2

    axis2 = W.HBox([W.Label(value="Horizontal axis", layout=lo_label),
                    widget['label2'],
                    widget['unit2'],
                    W.Label(value="origin / sampling / samples", layout=lo_label2),
                    widget['o2'],
                    widget['d2'],
                    n2clone])
    # Window
    lo_wind = small
    widget['x1min'] = W.FloatText(layout=lo_wind, value = x1min)
    widget['x1max'] = W.FloatText(layout=lo_wind, value = x1max)
    widget['x2min'] = W.FloatText(layout=lo_wind, value = x2min)
    widget['x2max'] = W.FloatText(layout=lo_wind, value = x2max)
    widget['cbmin'] = W.FloatText(layout=lo_wind, value = cbmin)
    widget['cbmax'] = W.FloatText(layout=lo_wind, value = cbmax)
    widget['clearw'] = W.Button(tooltip="Clear window range",
                                icon="trash", layout = lo_iconbt)

    widget['clearw'].on_click(functools.partial(on_clearw_clicked, widget=widget))

    window = W.HBox([W.Label(value="Window", layout=lo_label),
                     W.Label(value="vertical range:", layout=lo_label3),
                     widget['x1min'],
                     widget['x1max'],
                     W.Label(value="horizontal range:", layout=lo_label4),
                     widget['x2min'],
                     widget['x2max'],
                     W.Label(value="amplitude range:", layout=lo_label4),
                     widget['cbmin'],
                     widget['cbmax'],
                     widget['clearw']])

    # Further options
    widget['width'] = W.IntText(value=width, layout=small)
    widget['height'] = W.IntText(value=height, layout=small)

    widget['pdfwidth'] = W.FloatText(value=pdfwidth, step=0.5, layout=small)
    widget['pdfheight'] = W.FloatText(value=pdfheight, step=0.5, layout=small)

    widget['dialog'] = W.Checkbox(description="Interactive",
                                  value=dialog,
                                  layout = lo_check, indent=False)

    controls = W.HBox([W.Label(value="Image size (w/h)",layout=lo_label),
                       widget['width'],
                       widget['height'],
                       W.Label(value="Size of pdf (cm)",
                               layout=W.Layout(width="100px",
                                               display="flex",
                                               justify_content="flex-end")),
                       widget['pdfwidth'],
                       widget['pdfheight'],
                       widget['dialog']])

    # Scalebar
    widget['scalebar'] = W.Checkbox(description="Show scalebar", value=scale_bar,
                                    layout = lo_check, indent=False)

    options=[('gray', 1),\
             ('gray inverse', 2),\
             ('bent/cool/warm', 3),\
             ('bright blue/white/bright red', 4),\
             ('bright red/white/bright blue', 5),\
             ('viridis', 6),\
             ('parula', 7),\
             ('sand', 8),\
             ('white/yellow/red', 9),\
             ('turbo', 10),\
             ('jet', 11),\
             ('inferno', 12),\
             ('magma', 13),\
             ('red/yellow/blue', 14),\
             ('puor', 15),\
             ('spectral', 16),\
             ('purples', 17),\
             ('plasma', 18),\
             ('bones', 19),\
             ('blues', 20)]

    widget['palette'] = W.Dropdown(options=options, value=palette, layout=medium)

    widget['scalelabel'] = W.Text(placeholder="Label", layout=medium)
    if clabel is not None:
        widget['scalelabel'].value = clabel

    scale = W.HBox([widget['scalebar'],
                    widget['scalelabel'],
                    widget['palette']])


    widget['configp'] = W.VBox([controls,titleline,scale,axis1,axis2,window])
    widget['configp'].layout.display = 'none'

    # image area
    widget['imgw'] = W.HBox(layout=full)

    # toolbar
    toolbar = W.HBox([widget['input'],
                      widget['reload'],
                      widget['n1'],
                      widget['n2'],
                      widget['config'],
                      widget['plot'],
                      widget['pdf']], layout=full)


    # statusbar
    widget['statusbar'] = W.HTML()
    widget['statusbar'].layout = W.Layout(width="98%",
                                          border = "1px solid",
                                          padding = "2px")
    widget['statusbar'].add_class("statusbar")
    widget['statusbar'].layout.display = 'none'

    widget['main'] = W.VBox([toolbar,
                             widget['configp'],
                             widget['imgw'],
                             widget['statusbar']])

    if path.exists(data):
        on_reload_clicked(widget['reload'], widget)
    else:
        widget['statusbar'].value = f"<b>Last error message:</b> {data} not found"
        widget['statusbar'].layout.display = 'flex'

    display(HTML("""
       <style>
         .statusbar {border-radius: 4px; background: #f0f0f0;}
       </style>"""))

    if gui:
        display(widget['main'])

    on_plot_clicked(widget['plot'], widget, False, gui)

#--------------------------------------------------------------------------------
def ggraph(data, binary=True,title=None,
           d1=1, o1=0,
           label1=None, unit1=None,
           label2=None, unit2=None,
           x1min=math.nan, x1max=math.nan,
           x2min=math.nan, x2max=math.nan,
           marker=2, style=0, linewidth=1, color="#000000",
           tic1=math.nan, tic2=math.nan,
           width=1024, height=420, pdfwidth=12, pdfheight=6,
           dotted=False, grid1=False, grid2=False,
           dialog=False, gui=True):

    def parse_rsf(rsffn):

        pattern = re.compile('^[ 	]*([a-z].*)=(.*)$')
        lines = open(rsffn).readlines()
        header = {}
        for line in lines:
            match = pattern.match(line)
            if match:
                header[match.group(1)] = match.group(2)

        return header

    def on_config_clicked(button, widget):

        if widget['configp'].layout.display == 'none':
            widget['configp'].layout.display = 'flex'
            button.icon = 'caret-up'
        else:
            widget['configp'].layout.display = 'none'
            button.icon = 'caret-down'

    def on_binary_changed(change, widget):

        widget['o1'].disabled = not change['new']
        widget['d1'].disabled = not change['new']

    def on_reload_clicked(button, widget):

        fname = widget['input'].value
        if (not fname) or fname[-4:] != '.rsf':
            return

        if not path.exists(fname):
            widget['statusbar'].value = f"<b>Last error message:</b> {fname} not found"
            widget['statusbar'].layout.display = 'flex'
            return

        widget['header'] = parse_rsf(fname)

        for floatkey in ['d1','o1']:
            if floatkey in widget['header']:
                widget[floatkey].value = float(widget['header'][floatkey])

        for strkey in ['label1','label2','unit1','unit2']:
            if strkey in widget['header']:
                widget[strkey].value = widget['header'][strkey].replace('"', '')

    def on_clearw_clicked(button, widget):

        widget['x1min'].value = math.nan
        widget['x1max'].value = math.nan
        widget['x2min'].value = math.nan
        widget['x2max'].value = math.nan

    def on_plot_clicked(button, widget, pdf, gui):
        if not widget['input'].value:
            return

        if not path.exists(widget['input'].value):
            widget['statusbar'].value = f"<b>Last error message:</b> {widget['input'].value} not found"
            widget['statusbar'].layout.display = 'flex'
            return

        if (pdf and widget['dialog'].value):
            return

        if widget['input'].value[-4:] == '.rsf':
            fname = widget['header']['in'].replace('"', '')
        else:
            fname = widget['input'].value

        if not path.exists(fname):
            widget['statusbar'].value = f"<b>Last error message:</b> {fname} not found"
            widget['statusbar'].layout.display = 'flex'
            return

        if widget['input'].value != '/':
            cwd = getcwd()
        else:
            cwd = ''

        if pdf:
            fn = widget['input'].value + '.pdf'
            output = path.join(cwd,fn)
            width = widget['pdfwidth'].value
            height = widget['pdfheight'].value
            widget['statusbar'].value = 'Export to <a href="file://' + \
                                        output + '">' + fn + '</a>'
            widget['statusbar'].layout.display = 'flex'
        else:
            fnsvg = widget['input'].value + '.svg'
            fnpng = widget['input'].value + '.png'
            outputsvg = path.join(cwd,fnsvg)
            outputpng = path.join(cwd,fnpng)
            width = widget['width'].value
            height = widget['height'].value
            if not widget['dialog'].value:
                widget['statusbar'].value = 'Saved as <a href="file://' + \
                                            outputsvg + '">' + fnsvg + '</a> ' + \
                                            'and exported to <a href="file://' + \
                                            outputpng + '">' + fnpng + '</a>'
                widget['statusbar'].layout.display = 'flex'
            else:
                widget['statusbar'].value = ''
                widget['statusbar'].layout.display = 'none'

        cmdline = f"ggraph --data={fname} " + \
            f"--output={widget['input'].value} " +\
            f"--o1={widget['o1'].value} --d1={widget['d1'].value} " + \
            f"--label1=\"{widget['label1'].value}\" --label2=\"{widget['label2'].value}\" " + \
            f"--title=\"{widget['title'].value}\" " + \
            f"--linewidth={widget['lw'].value} --style={widget['ls'].value} "+\
            f"--marker={widget['marker'].value} --color=\"{widget['color'].value}\" "+\
            f"--width={width} --height={height} "

        if not widget['binary'].value:
            cmdline = cmdline + '--text '

        if widget['grid1'].value:
            cmdline = cmdline + '--grid1 '

        if widget['grid2'].value:
            cmdline = cmdline + '--grid2 '

        if pdf:
            cmdline = cmdline + '--pdf '

        if widget['unit1'].value:
            cmdline = cmdline + f"--unit1=\"{widget['unit1'].value}\" "

        if widget['unit2'].value:
            cmdline = cmdline + f"--unit2=\"{widget['unit2'].value}\" "

        if not math.isnan(widget['x1min'].value):
            cmdline = cmdline + f"--x1min={widget['x1min'].value} "

        if not math.isnan(widget['x1max'].value):
            cmdline = cmdline + f"--x1max={widget['x1max'].value} "

        if not math.isnan(widget['x2min'].value):
            cmdline = cmdline + f"--x2min={widget['x2min'].value} "

        if not math.isnan(widget['x2max'].value):
            cmdline = cmdline + f"--x2max={widget['x2max'].value} "

        if not math.isnan(widget['tic1'].value):
            cmdline = cmdline + f"--tic1={widget['tic1'].value} "

        if not math.isnan(widget['tic2'].value):
            cmdline = cmdline + f"--tic2={widget['tic2'].value} "

        if widget['dialog'].value:
            cmdline = cmdline + "--interactive "

        if widget['dotted'].value:
            cmdline = cmdline + "--dotted "

        run = subprocess.run([cmdline], shell=True, text=False)

        if not pdf and gui and not widget['dialog'].value:
            if len(widget['imgw'].children) == 0:
                widget['image'] = W.Image(layout = W.Layout(width="100%"))
                widget['imgw'].children = (widget['image'],)

#            widget['image'].format = 'svg+xml'
#            widget['image'].value = open(widget['input'].value+".svg","rb").read()

            widget['image'].format = 'png'
            widget['image'].value = open(widget['input'].value+".png","rb").read()

#-----------------------------------------------------------------------------

    widget = {}
    widget['header'] = {}

    full = W.Layout(width="98%")
    small = W.Layout(width="70px")
    medium = W.Layout(width="180px")
    lo_iconbt = W.Layout(width="30px", indent=False)
    lo_label = W.Layout(width="120px")
    lo_label2 = W.Layout(width="140px", display="flex", justify_content="flex-end", indent=False)
    lo_label3 = W.Layout(width="90px", display="flex", justify_content="flex-end")
    lo_label4 = W.Layout(width="120px", display="flex", justify_content="flex-end")
    lo_check = W.Layout(width="110px", display="flex", justify_content="flex-start")

    # binary or ascii data?
    widget['binary'] = W.Checkbox(description="binary", value=binary,
                                  layout=W.Layout(width="80px",
                                                  display="flex",
                                                  justify_content="flex-start"),
                                  indent=False)
    widget['binary'].observe(functools.partial(on_binary_changed, widget=widget), names='value')

    # data file
    widget['input'] = W.Text(placeholder="RSF or Binary data file",
                             value = data, layout=medium)

    # reload from rsf button
    widget['reload'] = W.Button(tooltip="Reload header info for RSF files",
                                icon="refresh", layout = lo_iconbt)
    widget['reload'].on_click(functools.partial(on_reload_clicked, widget=widget))

    # title
    widget['title'] = W.Text(placeholder="Title", layout=medium)
    if title is not None:
        widget['title'].value = title

    # config button
    widget['config'] = W.Button(description="Config",
                                button_style="info",
                                icon="caret-down",
                                indent=False)
    widget['config'].on_click(functools.partial(on_config_clicked, widget=widget))

    # plot button
    widget['plot'] = W.Button(description="Plot", button_style="danger",indent=False)
    widget['plot'].on_click(functools.partial(on_plot_clicked, widget=widget, pdf=False, gui=gui))

    # plot button
    widget['pdf'] = W.Button(description="Export to PDF", button_style="danger",indent=False)
    widget['pdf'].on_click(functools.partial(on_plot_clicked, widget=widget, pdf=True, gui=gui))

    # toolbar
    toolbar = W.HBox([widget['binary'],
                      widget['input'],
                      widget['reload'],
                      widget['title'],
                      widget['config'],
                      widget['plot'],
                      widget['pdf']], layout=full)

    #----------------------------------------------------------------------------
    # Some general options
    widget['width'] = W.IntText(value=width, layout=small)
    widget['height'] = W.IntText(value=height, layout=small)

    widget['pdfwidth'] = W.FloatText(value=pdfwidth, step=0.5, layout=small)
    widget['pdfheight'] = W.FloatText(value=pdfheight, step=0.5, layout=small)

    widget['dialog'] = W.Checkbox(description="Interactive",
                                  value=dialog,
                                  layout = lo_check, indent=False)

    controls = W.HBox([W.Label(value="Image size (w/h)",layout=lo_label),
                       widget['width'],
                       widget['height'],
                       W.Label(value="Size of pdf (cm)",
                               layout=W.Layout(width="100px",
                                               display="flex",
                                               justify_content="flex-end")),
                       widget['pdfwidth'],
                       widget['pdfheight'],
                       widget['dialog']])

    #----------------------------------------------------------------------------
    # Axis 1
    widget['o1'] = W.FloatText(layout=small, value = o1)
    widget['d1'] = W.FloatText(layout=small, value = d1)
    widget['tic1'] = W.FloatText(layout=small, value = tic1)
    widget['label1'] = W.Text(placeholder="Label",layout=medium)
    if label1 is not None:
        widget['label1'].value = label1

    widget['unit1'] = W.Text(placeholder="Unit",layout=medium)
    if unit1 is not None:
        widget['unit1'].value = unit1

    widget['grid1'] = W.Checkbox(description="grid",value=grid1,layout=lo_check,indent=False)

    if not binary:
        widget['o1'].disabled = True
        widget['d1'].disabled = True

    axis1 = W.HBox([W.Label(value="Horizontal axis", layout=lo_label),
                    widget['label1'],
                    widget['unit1'],
                    widget['grid1'],
                    W.Label(value="origin / sampling / tic int", layout=lo_label2),
                    widget['o1'],
                    widget['d1'],
                    widget['tic1']])

    #----------------------------------------------------------------------------
    # Axis 2
    widget['tic2'] = W.FloatText(layout=small, value = tic2)
    widget['label2'] = W.Text(placeholder="Label",layout=medium)
    if label2 is not None:
        widget['label2'].value = label2

    widget['unit2'] = W.Text(placeholder="Unit",layout=medium)
    if unit2 is not None:
        widget['unit2'].value = unit2
        
    widget['grid2'] = W.Checkbox(description="grid",
                                 value=grid2,
                                 layout=lo_check,
                                 indent=False)

    axis2 = W.HBox([W.Label(value="Vertical axis", layout=lo_label),
                    widget['label2'],
                    widget['unit2'],
                    widget['grid2'],
                    W.Label(value="tic int", layout=lo_label2),
                    widget['tic2']])

    #----------------------------------------------------------------------------
    # Style
    widget['lw'] = W.FloatText(value=linewidth, step=0.5, layout=small)

    loptions =[('line', 0),('marks', 1),('line and marks',2),('smooth line',3)]
    widget['ls'] = W.Dropdown(options=loptions,value=style, layout=lo_label)

    widget['dotted'] = W.Checkbox(description="dotted line",
                                  value=dotted,
                                  layout=lo_check,
                                  indent=False)

    widget['marker'] = W.Dropdown(options=[('dot', 0),
                                           ('plus', 1),
                                           ('times',2),
                                           ('times/plus',3),
                                           ('square',4),
                                           ('filled square',5),
                                           ('circle',6),
                                           ('filled circle',7),
                                           ('up triangle',8),
                                           ('filled up triangle',9),
                                           ('down triangle', 10),
                                           ('filled down triangle',11),
                                           ('diamond', 12),
                                           ('filled diamond', 13)],
                                  value=marker,
                                  layout=lo_label)
    widget['color'] = W.ColorPicker(concise=True, value=color)
    
    style = W.HBox([W.Label(value="Trace style", layout=lo_label),
                    W.Label(value="line width:", layout=lo_label3),
                    widget['lw'],
                    widget['ls'],
                    widget['dotted'],
                    W.Label(value="marker:", layout=lo_label4,indent=False),
                    widget['marker'],
                    widget['color']])
    
    #----------------------------------------------------------------------------
    # Window
    lo_wind = small
    widget['x1min'] = W.FloatText(layout=lo_wind, value = x1min)
    widget['x1max'] = W.FloatText(layout=lo_wind, value = x1max)
    widget['x2min'] = W.FloatText(layout=lo_wind, value = x2min)
    widget['x2max'] = W.FloatText(layout=lo_wind, value = x2max)
    widget['clearw'] = W.Button(tooltip="Clear window range",
                                icon="trash", layout = lo_iconbt)

    widget['clearw'].on_click(functools.partial(on_clearw_clicked, widget=widget))

    window = W.HBox([W.Label(value="Window", layout=lo_label),
                     W.Label(value="vertical range:", layout=lo_label3),
                     widget['x2min'],
                     widget['x2max'],
                     W.Label(value="horizontal range:", layout=lo_label4),
                     widget['x1min'],
                     widget['x1max'],
                     widget['clearw']])

    #----------------------------------------------------------------------------
    widget['configp'] = W.VBox([controls,axis2,axis1,style,window])
    widget['configp'].layout.display = 'none'

    #----------------------------------------------------------------------------
    # image area
    widget['imgw'] = W.HBox(layout=full)

    #----------------------------------------------------------------------------
    # statusbar
    widget['statusbar'] = W.HTML()
    widget['statusbar'].layout = W.Layout(width="98%",
                                          border = "1px solid",
                                          padding = "2px")
    widget['statusbar'].add_class("statusbar")
    widget['statusbar'].layout.display = 'none'

    #----------------------------------------------------------------------------
    widget['main'] = W.VBox([toolbar,
                             widget['configp'],
                             widget['imgw'],
                             widget['statusbar']])

    if path.exists(data):
        on_reload_clicked(widget['reload'], widget)
    else:
        widget['statusbar'].value = f"<b>Last error message:</b> {data} not found"
        widget['statusbar'].layout.display = 'flex'

    display(HTML("""
       <style>
         .statusbar {border-radius: 4px; background: #f0f0f0;}
       </style>"""))

    if gui:
        display(widget['main'])

    on_plot_clicked(widget['plot'], widget, False, gui)


#-----------------------------------------------------------------------------
def gshow(image):

    widget = W.Image(layout = W.Layout(width="100%"))

    if image[-4:] == '.png':
        widget.format = 'png'
    elif image[-4:] == '.svg':
        widget.format = 'svg+xml'
    elif image[-4:] == '.jpg' or image[-5:] == '.jpeg':
        widget.format = 'jpg'
    else:
        print("Unknwon image extension")
        return None

    widget.value = open(image,"rb").read()
    return widget
