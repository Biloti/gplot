__version__ = "0.1.0"
__author__ = ["Ricardo Biloti, biloti@unicamp.br"]
__license__ = "GPL 3.0"

from .gplot import gimage, gwiggle, ggraph, gshow
