# GPlot

A tool to export images or wiggle plots for single-precision data sets.
Gplot support PDF, SVG and PNG formats, using Gnuplot as backend.
Furthermore, a Python tool is provided to display those images inside
jupyter notebooks.

## Install

Gplot depends on Glib and Gnuplot. On Debian-based Linux distros, those
dependencies are installed with

```
sudo install libglib2.0-dev gnuplot
```

To download, build and install gplot, run

```
git clone https://gitlab.com/Biloti/gplot.git
cd gplot
make install
```

