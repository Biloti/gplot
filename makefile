PYTHON=/usr/bin/python3
PIP=/usr/bin/pip3

ALL: src gplot

install: $(ALL)

src:
	cd src && make install

gplot:
	$(PYTHON) -m venv .venv
	$(PIP) install --editable .

.PHONY: gplot src
